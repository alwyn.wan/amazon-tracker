const express = require('express');
const app = express();
const morgan = require('morgan');
const body_parser = require('body-parser');
const cors = require('cors');

const prices = require('./routes/prices');
const products = require('./routes/products');

app.use(cors());
app.use(morgan('short'));
app.use(body_parser.urlencoded({ extended: false }));
app.use(body_parser.json());
app.use(prices);
app.use(products);

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next();
});

app.get('/', (req, res) => {
	console.log('Responding to root route');
	res.send('Hello from ROOT!');
});

app.listen(8080, () => {});
