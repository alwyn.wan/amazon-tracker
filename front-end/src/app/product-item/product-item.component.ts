import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../models/product';
import { ProductidService } from '../services/productid.service';

@Component({
	selector: 'app-product-item',
	templateUrl: './product-item.component.html',
	styleUrls: [ './product-item.component.css' ]
})
export class ProductItemComponent implements OnInit {
	@Input() product: Product;
	@Output() releaseProduct = new EventEmitter<Product>();

	active: boolean;

	constructor(private idService: ProductidService) {}

	ngOnInit() {
		this.active = false;
	}

	setClasses() {
		return {
			button: true,
			product: true,
			active: this.idService.getActiveProduct() === this.product.productId
		};
	}

	setActiveProduct(pId: number) {
		this.idService.setActiveProduct(pId);
		this.releaseProduct.emit(this.product);
	}
}
