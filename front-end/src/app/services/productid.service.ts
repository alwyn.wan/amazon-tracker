import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class ProductidService {
	activeProduct: number;
	constructor() {}

	setActiveProduct(pId: number): void {
		this.activeProduct = pId;
	}

	getActiveProduct(): number {
		return this.activeProduct;
	}
}
