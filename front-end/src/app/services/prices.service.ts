import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Price } from '../models/price';

@Injectable({
	providedIn: 'root'
})
export class PricesService {
	apiUrl: string = 'http://localhost:8080';

	constructor(private http: HttpClient) {}

	getPrices(): Observable<Price[]> {
		return this.http.get<Price[]>(this.apiUrl + '/prices');
	}

	getPricesForProduct(pId: number): Observable<any[]> {
		return this.http.get<any[]>(this.apiUrl + '/prices/' + pId);
	}

	refreshPrices(): Observable<any> {
		return this.http.post<any>(this.apiUrl + '/prices/', {});
	}
}
