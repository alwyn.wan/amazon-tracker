export class Price {
	id: number;
	dateStamp: Date;
	productId: number;
	price: number;
}
